﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace fr_stringcalculator
{
    public class Calculator
    {
        private const int DEFAULT_VALUE_RESULT_FOR_EMPTY_NUMBERS = 0;
        private const string CUSTOM_DELIMITER = "//";
        private List<string> DELIMITERS = new List<string>() { ",", "\n" };

        /*
        Candidate to include some patherns like Strategy or some things.... 
        */
        public int Add(string numbers)
        {
            if (String.IsNullOrWhiteSpace(numbers))
            {
                return DEFAULT_VALUE_RESULT_FOR_EMPTY_NUMBERS;
            }
            else if (numbers.StartsWith(CUSTOM_DELIMITER))
            {
                numbers = AddTheCustomDelimiter(numbers);
            }

            var numbersConvertedToNumbers = ParseStringNumbersToNumbers(numbers);

            ValidateNegativeNumber(numbersConvertedToNumbers);

            return Sum(numbersConvertedToNumbers);
        }


        /*
        AddTheCustomDelimiter: This method add a custom delimiter to the string numbers 
        */
        private string AddTheCustomDelimiter(string numbers)
        {
            string[] customSeperators = { CUSTOM_DELIMITER, "[", "]" };

            var customDelimiter = numbers
                .Split(new string[] { "\n" }, StringSplitOptions.RemoveEmptyEntries)
                .FirstOrDefault();

            numbers = numbers.Substring(customDelimiter.Length, numbers.Length - customDelimiter.Length);

            var customsDelimiters = customDelimiter
                .Split(customSeperators, StringSplitOptions.RemoveEmptyEntries);

            foreach (var sep in customsDelimiters)
            {
                DELIMITERS.Add(sep);
            }

            return numbers;
        }

        /*
         ParseStringNumbersToNumbers: This method convert the string numbers to a array of int 
        */
        public int[] ParseStringNumbersToNumbers(string numbers)
        {
            var splitedNumbers = numbers
                    .Split(DELIMITERS.ToArray(), StringSplitOptions.RemoveEmptyEntries) // The StringSplitOptions.RemoveEmptyEntries is important to work with string delimiters. More information
                                                                                        // https://msdn.microsoft.com/es-es/library/ms131448(v=vs.110).aspx
                    .Select(int.Parse).ToArray();

            return splitedNumbers;
        }

        public void ValidateNegativeNumber(int[] numbersConvertedToNumbers)
        {
            var negativeNumbers = new System.Text.StringBuilder();
            foreach (int negativeNumber in numbersConvertedToNumbers)
            {
                if (negativeNumber < 0)
                {
                    negativeNumbers.Append(negativeNumber).Append(".");
                }
            }

            if (negativeNumbers.Length > 0)
            {
                throw new System.ArgumentException("Negatives not allowed: ", negativeNumbers.ToString());
            }
        }

        public int Sum(int[] numbers)
        {
            return numbers.Sum();
        }
    }
}