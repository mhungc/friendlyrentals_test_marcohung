﻿using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace fr_stringcalculator.Tests
{
    [TestFixture]
    public class CalculatorTest
    {
        private Calculator calculator;

        [SetUp]
        public void Setup()
        {
            calculator = new Calculator();
        }

        [Test]
        public void add_emptystring_zero()
        {
            var calculatorResult = calculator.Add("");
            Assert.That(calculatorResult, Is.EqualTo(0));
        }

        [Test]
        [TestCase("1",1)]
        [TestCase("3",3)]
        public void add_sumOneNumber_oneNumber(string expected,int arg)
        {
            Assert.That(calculator.Add(expected),Is.EqualTo(arg));
        }

        [Test]
        [TestCase("1,2", 3)] //-> Test for Two Numbers
        [TestCase("3,1,5", 9)] // -> Test for unknown amount of numbers
        [TestCase("3\n2,5", 10)] // -> Test for \n
        [TestCase("//;\n1;2", 3)] // -> Test for custom delimiter
        public void add_sumNumbers_addedNumbers(string expected, int arg)
        {
            Assert.That(calculator.Add(expected), Is.EqualTo(arg));
        }

        [Test]
        [TestCase("1,2", 3)]
        [TestCase("1,-2", 3)]
        [TestCase("1,-2,-4", 3)]
        public void add_negativeNumber_addedNumbers(string expected, int arg)
        {
            Assert.That(calculator.Add(expected), Is.EqualTo(arg));
        }
    }
}
